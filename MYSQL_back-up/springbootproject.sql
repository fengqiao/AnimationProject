/*
 Navicat Premium Data Transfer

 Source Server         : locallhost
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : localhost:3306
 Source Schema         : website

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 02/06/2023 14:09:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for comic_comment
-- ----------------------------
DROP TABLE IF EXISTS `comic_comment`;
CREATE TABLE `comic_comment`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `comicId` int NULL DEFAULT NULL,
  `userId` int NULL DEFAULT NULL,
  `commentId` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `createTime` bigint NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comic_comment
-- ----------------------------
INSERT INTO `comic_comment` VALUES (13, 21, 19, '26', 1681003990941);
INSERT INTO `comic_comment` VALUES (14, 12, 19, '27', 1681004017635);
INSERT INTO `comic_comment` VALUES (23, 16, 19, '45', 1683360213180);
INSERT INTO `comic_comment` VALUES (24, 12, 19, '46', 1685682065686);
INSERT INTO `comic_comment` VALUES (25, 21, 19, '47', 1685683728223);

-- ----------------------------
-- Table structure for comic_table
-- ----------------------------
DROP TABLE IF EXISTS `comic_table`;
CREATE TABLE `comic_table`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '动漫的id',
  `comicName` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '动漫的名字',
  `nickname` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '动漫的名字（别名）',
  `cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '封面',
  `region` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '地区',
  `label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '标签',
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '描述',
  `remark` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '备注',
  `year` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '年份',
  `updateTime` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '更新时间',
  `number` int NULL DEFAULT NULL COMMENT '集数',
  `popularity` int NULL DEFAULT NULL COMMENT '人气',
  `url` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '外部链接',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comic_table
-- ----------------------------
INSERT INTO `comic_table` VALUES (12, '海贼王', 'One Piece', '/photo/5a3d3d40-18cd-497f-b20f-eaaccfdf1f5f4.jpg', '日本', '热血冒险', '大海贼时代！', '更新至1070话', '1997', '2022-12-21', 1070, 999999999, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (14, '斗罗大陆第一季', '斗罗大陆动画版', '/photo/a7e35fd2-c23a-4219-a62a-1a6eeccae6e36.jpeg', '大陆', '奇幻 玄幻 穿越 热血 战斗 国产动漫  ', '网络动画《斗罗大陆》改编自中国作家唐家三少原作的同名玄幻小说，由玄机科技制作，', '更新至240集', '2018 ', '2022-12-24', 240, 7888888, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (15, '名侦探柯南', '名侦探柯南 日语版/名探侦コナン Detective Conan', '/photo/a3a284f9-2e43-44b7-bde3-095309bd85bf5.jpg', '日本', '悬疑 推理 犯罪 日本动漫  ', '电视动画《名侦探柯南》改编自青山刚昌创作的、连载于《周刊少年Sunday》上的漫画《名侦探柯南》。', '更新至1124集', '2006 ', '2022-12-24', 1124, 88888888, 'httpsxxx');
INSERT INTO `comic_table` VALUES (16, '咒术回战', '呪術廻戦', '/photo/272b6411-9664-44e9-bcc2-ae6626caca4b7.jpg', '日本', '奇幻 冒险 战斗 日本动漫', '电视动画《咒术回战》改编自芥见下下著作的同名漫画，于2019年11月25日在《周刊少年Jump》52号上发表了动画化的消息。', '完结', '2020 ', '2022-11-02', 24, 4165781, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (17, '火影忍者：博人传之次世代继承者', '博人传 火影忍者新时代', '/photo/4a57bc0c-dc38-4b98-b47b-4cbc4f6bedf73.jpg', '日本', '冒险 奇幻 热血 日本动漫', '日本电视动画《BORUTO -火影新世代-》（中国大陆译名《博人传：火影忍者新时代》）改编自岸本齐史原作并监修、池本干雄编绘', '更新至280集', '2017', '2022-12-24', 280, 6547893, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (18, '终末的女武神', 'Record of Ragnarok', '/photo/7de3b8d4-46fc-4ebf-8d6c-4723d014d3798.jpg', '日本', '战斗 热血 剧情 日本动漫 ', '预计在 2021 年开始播映的电视动画《终末的女武神》，官方就在 15 日这天释出首波视觉图、追加角色&声优以及动画主题曲等情报', '完结', '2021 ', '2022-11-02', 12, 4569842, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (19, '秒速五厘米', '秒速5厘米', '/comic/1685684037420_OIP-C.jpg', '日本', '爱情 日本动漫', '樱花下落的时间是每秒5厘米，我与你的相见又在何时', '完结', '2020 ', '2007-10-24', 1, 652165, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (20, '战斗员派遣中！', '派遣战斗员/戦闘員、派遣します！', '/photo/d29eef1f-7069-4d0a-a47b-7353e8b661b51.jpg', '日本', '奇幻 冒险 战斗 日本动漫 ', '电视动画《战斗员派遣中！》改编自晓なつめ著作，カカオ・ランタン负责插画的同名轻小说作品，于2020年3月15日宣布了动画化的消息。', '完结', '2021 ', '2021-08-31', 12, 985613, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (21, '无职转生：到了异世界就拿出真本事 第二季', '无职转生 ～在异世界认真地活下去～ 第二季', '/photo/59a707aa-c726-4aa0-9d5a-9b7a3852d43c10.jpg', '日本', '奇幻 冒险 后宫 战斗 日本动漫', '奇幻 冒险 后宫 战斗 日本动漫', '完结+OVA', '2021', '2022-04-12', 14, 9856132, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (22, '镇魂街第二季', '镇魂街第2季', '/photo/2000dec7-ef83-487a-834c-97320369f9032.jpg', '大陆', '奇幻 热血 战斗 国产动漫 ', '时隔3年之后，无数粉丝翘首以盼的《镇魂街》动画第二季终于是来了，此番第二季一开播就播出了两集，', '完结', '2019 ', '2021-06-26', 10, 9856213, 'httpsxxxx');
INSERT INTO `comic_table` VALUES (27, '赛博朋克', '赛博朋克', '/comic/1681024440905_saibo.jpg', '日本', '科幻、战斗、爱情', '夜之城没有活着的传奇~', '已完结', '2022', '2022-09-22', 10, 9999, ' httpsxxxxx');

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'id',
  `comment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '评论内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES (26, '123');
INSERT INTO `comment` VALUES (27, '123');
INSERT INTO `comment` VALUES (38, '333');
INSERT INTO `comment` VALUES (39, '2222');
INSERT INTO `comment` VALUES (41, '3123');
INSERT INTO `comment` VALUES (43, '32321');
INSERT INTO `comment` VALUES (44, '3232');
INSERT INTO `comment` VALUES (45, '1111');
INSERT INTO `comment` VALUES (46, '111');
INSERT INTO `comment` VALUES (47, '333');
INSERT INTO `comment` VALUES (48, '你好呀');

-- ----------------------------
-- Table structure for comment_reply_content
-- ----------------------------
DROP TABLE IF EXISTS `comment_reply_content`;
CREATE TABLE `comment_reply_content`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `commentId` int NULL DEFAULT NULL,
  `replyId` int NULL DEFAULT NULL,
  `repliedId` int NULL DEFAULT NULL,
  `createTime` bigint NULL DEFAULT NULL,
  `comicCommentId` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comment_reply_content
-- ----------------------------
INSERT INTO `comment_reply_content` VALUES (10, 38, 22, 19, 1681024048339, 13);
INSERT INTO `comment_reply_content` VALUES (11, 39, 20, 22, 1681024089984, 10);
INSERT INTO `comment_reply_content` VALUES (13, 41, 20, 22, 1681024106572, 10);
INSERT INTO `comment_reply_content` VALUES (15, 43, 20, 22, 1681024164703, 10);
INSERT INTO `comment_reply_content` VALUES (16, 44, 20, 19, 1681024176512, 13);
INSERT INTO `comment_reply_content` VALUES (17, 48, 22, 19, 1685683794800, 25);

-- ----------------------------
-- Table structure for user_collection
-- ----------------------------
DROP TABLE IF EXISTS `user_collection`;
CREATE TABLE `user_collection`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `comic_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_collection
-- ----------------------------

-- ----------------------------
-- Table structure for user_history
-- ----------------------------
DROP TABLE IF EXISTS `user_history`;
CREATE TABLE `user_history`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `comic_id` int NOT NULL,
  `user_id` int NOT NULL,
  `create_time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_history
-- ----------------------------
INSERT INTO `user_history` VALUES (40, 12, 19, '1685683669546');
INSERT INTO `user_history` VALUES (41, 18, 19, '1685683710526');
INSERT INTO `user_history` VALUES (42, 21, 19, '1685683724509');
INSERT INTO `user_history` VALUES (43, 22, 22, '1685683764323');
INSERT INTO `user_history` VALUES (44, 21, 22, '1685683787843');
INSERT INTO `user_history` VALUES (45, 19, 19, '1685684056347');

-- ----------------------------
-- Table structure for user_table
-- ----------------------------
DROP TABLE IF EXISTS `user_table`;
CREATE TABLE `user_table`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `userName` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `createTime` bigint NULL DEFAULT NULL,
  `isAdmin` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user_table
-- ----------------------------
INSERT INTO `user_table` VALUES (18, '枫桥', '5f329d3ac22671f7b214c461e58c27f3', '/user/1685683895773_QQ图片20211225223455.jpg', '123', '123', 1685683896943, 1);
INSERT INTO `user_table` VALUES (19, '123', '5f329d3ac22671f7b214c461e58c27f3', '/user/1681014172786_23b4fd7d232e5450.jpg', '33333@123.com', '22222', 1685682387717, 0);
INSERT INTO `user_table` VALUES (20, '333', '1add994bc3f1c5955bbd09a6ae665388', '/user/1681024185292_6fb86ee066ca38ee.jpg', '333333', '332', 1681024185325, 0);
INSERT INTO `user_table` VALUES (22, '1', '922c0e7cc51bc744a3199eae471ab14f', NULL, '1', '1', 1681023923174, 0);

SET FOREIGN_KEY_CHECKS = 1;
